// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

contract HelloUniversePayable {

  string message = "Hello Universe!";

  function getMessage() public view returns (string memory) {
    return message;
  }

  function setMessage(string memory newMessage) public payable {
    message = newMessage;
  }

}
