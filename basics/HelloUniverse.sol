// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

contract HelloUniverse {

    string name;
    
    function set(string memory _name) public {
        name = _name;
    }

    function get() public view returns (string memory) {
        return name;
    }

    function hello() public pure returns(string memory){
        string memory message = "hello from Parallel Universe";
        return message;
    }
    
}
