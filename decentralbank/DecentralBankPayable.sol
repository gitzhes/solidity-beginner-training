// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

contract Ownable {
    address internal owner;

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    constructor() {
        owner = msg.sender;
    }
}


contract DecentralBankPayable is Ownable {

    mapping(address => uint) balance;
    
    event depositDone(uint amount, address indexed depositedTo);
    event withdrawalDone(uint amount, address indexed accountFrom);
    event transferDone(uint amount, address indexed fromAccount, address indexed toAccount);

    // BALANCE
    function getBalance() public view returns (uint) {
        return balance[msg.sender];
    }
    
    // DEPOSIT MONEY
    function deposit() public payable returns (uint)  {
        balance[msg.sender] += msg.value;

        emit depositDone(msg.value, msg.sender);
        
        return balance[msg.sender];
    }
    

    // WITHDRAW MONEY
    function withdraw() public onlyOwner payable returns (uint){
        require(balance[msg.sender] >= msg.value);

        balance[msg.sender] -= msg.value;
        payable(msg.sender).transfer(msg.value);

        emit withdrawalDone(msg.value, msg.sender);

        return balance[msg.sender];
    }
    
    
    // TRANSFER MONEY
    function transfer(address recipient, uint amount) public payable {
        require(balance[msg.sender] >= amount, "Balance not sufficient");
        require(msg.sender != recipient, "Don't transfer money to yourself");
        
        uint previousSenderBalance = balance[msg.sender];
        
        _transfer(msg.sender, recipient, amount);

        emit transferDone(amount, msg.sender, recipient);
    
        assert(balance[msg.sender] == previousSenderBalance - amount);
    }
    
    // Private function example
    function _transfer(address from, address to, uint amount) private {
        balance[from] -= amount;
        balance[to] += amount;
    }

}
