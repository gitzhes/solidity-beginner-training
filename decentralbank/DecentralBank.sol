// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

contract Ownable {
    address internal owner;

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    constructor() {
        owner = msg.sender;
    }
}

contract DecentralBank is Ownable {

    mapping(address => uint) balance;
    
    event depositDone(uint amount, address indexed depositedTo);
    event withdrawalDone(uint amount, address indexed creditedFrom);
    event transferDone(uint amount, address indexed fromAccount, address indexed toAccount);

    // BALANCE
    function getBalance() public view returns (uint){
        return balance[msg.sender];
    }
    
    // DEPOSIT MONEY
    function deposit(uint amount) public returns (uint)  {
        balance[msg.sender] += amount;
        
        emit depositDone(amount, msg.sender);
        
        return balance[msg.sender];
    }
    

    // WITHDRAW MONEY
    function withdraw(uint amount) public onlyOwner returns (uint) {
        require(balance[msg.sender] >= amount);
        
        balance[msg.sender] -= amount;

        emit withdrawalDone(amount, msg.sender);

        return balance[msg.sender];
    }
    
    
    // TRANSFER MONEY
    function transfer(address recipient, uint amount) public {
        require(balance[msg.sender] >= amount, "Balance is not sufficient");
        require(msg.sender != recipient, "Transfer money to yourself");
        
        uint previousSenderBalance = balance[msg.sender];
        
        _transfer(msg.sender, recipient, amount);

        emit transferDone(amount, msg.sender, recipient);
    
        assert(balance[msg.sender] == previousSenderBalance - amount);
    }
    
    // Private function example
    function _transfer(address from, address to, uint amount) private {

        balance[from] -= amount;
        balance[to] += amount;
    
    }

}