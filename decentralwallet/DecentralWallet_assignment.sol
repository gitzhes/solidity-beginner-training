// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

contract DecentralWallet {

    // Multisig Wallet
    // Rules :->
    // Any of those associates can create a new transfer
    // All associates should be able to approve the request
    // Once required signatures are done, the transfer is initiated


    // Help: State variables, such as list of owners and how many approvals
    address[] public owners;
    uint limit;
    
    // Help: Use struct to define transfers
    struct Transfer{
        uint amount;
        address payable receiver;
        uint approvals;
        bool hasBeenSent;
        uint id;
    }
    
    // Events for the interaction with the contract

    // Help :->
    // Collect all transfer requests
    // "Double-mapping" to find out who approved
    Transfer[] transferRequests;
    mapping(address => mapping(uint => bool)) approvals;
    
    // Help: Modifier to only allow addresses in the owners list
    modifier onlyOwners(){
        bool owner = false;
        for(uint i = 0; i < owners.length; i++) {
            if(owners[i] == msg.sender){
                owner = true;
            }
        }
        require(owner == true);
        _;
    }

    //Hint: Constructor to your state variables
    constructor(address[] memory _owners, uint _limit) {
        owners = _owners;
        limit = _limit;
    }
    
    // Deposit function
    function deposit() public payable {
      // Fill it up?!
    }
    
    // Create the transfer
    function createTransfer(uint _amount, address payable _receiver) public onlyOwners {
        // Hint: Use transfer struct and ...
    }
    
    // Approve the transfer request
    function approve(uint _id) public onlyOwners {
        // Check: Should not be able to vote twice.
        // Check: Should not be able to vote on a tranfer request that has already been sent.

        // Hint: 
        // Struct to be updated
        // Update of the mapping for approvals
        // Once it reaches the limit, start the transfer 
    }
    
    // Get all transfer requests
    function getTransferRequests() public view returns (Transfer[] memory) {
        // Fill it up
    }
    
    
}